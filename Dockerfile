FROM node:10.15.3-jessie

COPY --chown=node . /home/node/app

USER node

RUN mkdir /home/node/app/node_modules

CMD node
