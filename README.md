# boilerplate-typescript-jest-eslint

boilerplate repo for 
 - typescript
 - jest
 - eslint
 - an example test


## Getting Started
The `Dockerfile` and `docker-compose.yml` are designed for development

## Docker-based development
Dependencies:
 - Official node image
 - running node container as user=node

Start the development machine:

```
sudo docker-compose up
```

Enter the shell

```
sudo docker-compose exec node101 /bin/bash
```
